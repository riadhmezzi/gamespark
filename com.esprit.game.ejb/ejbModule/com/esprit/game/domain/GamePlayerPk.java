package com.esprit.game.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class GamePlayerPk implements Serializable{

	private int idGamePk;
	private int idPlayerPk;
	public int getIdGamePk() {
		return idGamePk;
	}
	public void setIdGamePk(int idGamePk) {
		this.idGamePk = idGamePk;
	}
	public int getIdPlayerPk() {
		return idPlayerPk;
	}
	public void setIdPlayerPk(int idPlayerPk) {
		this.idPlayerPk = idPlayerPk;
	}
	public GamePlayerPk(int idGamePk, int idPlayerPk) {
		super();
		this.idGamePk = idGamePk;
		this.idPlayerPk = idPlayerPk;
	}
	public GamePlayerPk() {
		super();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idGamePk;
		result = prime * result + idPlayerPk;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GamePlayerPk other = (GamePlayerPk) obj;
		if (idGamePk != other.idGamePk)
			return false;
		if (idPlayerPk != other.idPlayerPk)
			return false;
		return true;
	}
	
	
	
}
