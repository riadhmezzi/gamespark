package com.esprit.game.services;

import java.util.List;

import javax.ejb.Remote;

import com.esprit.game.domain.Game;
import com.esprit.game.domain.Personne;
import com.esprit.game.domain.Player;


@Remote
public interface GameServiceRemote {
	
	public void addGame(Game game);

	public void updateGame(Game game);

	public Game findGameByID(int id);

	public void deleteGame(Game game);

	public List<Game> findAllGames();
	
}
