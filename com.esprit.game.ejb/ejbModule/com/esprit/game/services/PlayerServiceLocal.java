package com.esprit.game.services;

import java.util.List;

import javax.ejb.Local;

import com.esprit.game.domain.Personne;
import com.esprit.game.domain.Player;


@Local
public interface PlayerServiceLocal {
	
	public void addPlayer(Player player);

	public void updatePlayer(Player player);

	public Player findPlayerByID(int id);

	public void deletePlayer(Player player);

	public List<Player> getPlayers();
	
	public List<Player> getPlayersByAge(int age);
	
	//public Player getPlayerByCIN(int cin);
	
	public Personne authenticate(String login, String password); 
	 
	public List<Player> findAllPlayers();
	
}
