package com.esprit.game.webJSF.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.esprit.game.domain.Game;
import com.esprit.game.services.GameServiceLocal;





@ManagedBean(name="GameMB")
@ViewScoped
public class GameManagedBean  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Game game = new Game();
	
	private List<Game> games = new ArrayList<Game>();
	private boolean displayList = false ; 
	
	public boolean isDisplayList() {
		return displayList;
	}


	public void setDisplayList(boolean displayList) {
		this.displayList = displayList;
	}





	private boolean displayForm = false ; 
	
	public boolean isDisplayForm() {
		return displayForm;
	}


	public void setDisplayForm(boolean displayForm) {
		this.displayForm = displayForm;
	}


	@PostConstruct
	public void init(){
		games = gameServiceLocal.findAllGames(); 
	}
	




	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public List<Game> getGames() {
		return games;
	}


	public void setGames(List<Game> games) {
		this.games = games;
	}





	@EJB
	private GameServiceLocal gameServiceLocal; 
	
	public String saveOrUpdate(){
		String navTo=null ; 
		
		gameServiceLocal.updateGame(game);
		games = gameServiceLocal.findAllGames(); 
		displayForm = false ; 
		return navTo ;
	}
	
	public String showForm(){
		String navTo=null ; 
		game = new Game() ; 
		displayForm= true ; 
		
		return navTo ;
	}
	
	
	public String delete(){
		String navTo=null ; 
		gameServiceLocal.deleteGame(game);
		games = gameServiceLocal.findAllGames();
		return navTo ;
	}
	
	public String cancel(){
		String navTo=null ; 
		displayForm = false; 
		return navTo ;
	}
	
	
public String showList(){
	displayList=true;
	return null;
}

public String hideList(){
	displayList=false;
	return null;
	
}

}
