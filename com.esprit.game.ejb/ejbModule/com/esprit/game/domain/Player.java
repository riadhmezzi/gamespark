package com.esprit.game.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Player extends Personne implements Serializable{

	
	private int age ; 
	private int coins ;
	
	@OneToMany(mappedBy = "player")
	private List<GamePlayer> gamePlayers = new ArrayList<GamePlayer>();
	
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getCoins() {
		return coins;
	}
	public void setCoins(int coins) {
		this.coins = coins;
	} 

	
	
	
}
