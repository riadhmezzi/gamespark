package com.esprit.game.domain;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class GamePlayer implements Serializable {

	@EmbeddedId
	private GamePlayerPk gamePlayerPk ; 
	
	private int bestScore; 
	private int achete; 
	
	@ManyToOne
	@JoinColumn(name = "idGamePk", referencedColumnName = "id", insertable = false, updatable = false)
	private Game game ; 
	

	@ManyToOne
	@JoinColumn(name = "idPlayerPk", referencedColumnName = "id", insertable = false, updatable = false)
	private Player player ; 
	
	
	
	
	
	public GamePlayerPk getGamePlayerPk() {
		return gamePlayerPk;
	}

	public void setGamePlayerPk(GamePlayerPk gamePlayerPk) {
		this.gamePlayerPk = gamePlayerPk;
	}

	public int getBestScore() {
		return bestScore;
	}

	public void setBestScore(int bestScore) {
		this.bestScore = bestScore;
	}

	public int getAchete() {
		return achete;
	}

	public void setAchete(int achete) {
		this.achete = achete;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public GamePlayer(int bestScore, int achete, Game game, Player player) {
		super();
		this.bestScore = bestScore;
		this.achete = achete;
		this.game = game;
		this.player = player;
		this.gamePlayerPk= new GamePlayerPk(game.getId(), player.getId());
	}

	public GamePlayer() {
		super();
	}

}
