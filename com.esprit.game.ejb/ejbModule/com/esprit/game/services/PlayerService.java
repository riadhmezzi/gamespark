package com.esprit.game.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.esprit.game.domain.Personne;
import com.esprit.game.domain.Player;

/**
 * Session Bean implementation class ClientService
 */
@Stateless
public class PlayerService implements PlayerServiceRemote, PlayerServiceLocal {

    @PersistenceContext(name="gamePU")
    EntityManager entityManager;

	@Override
	public void addPlayer(Player player) {
		// TODO Auto-generated method stub
		entityManager.persist(player);
	}

	@Override
	public void updatePlayer(Player player) {
		// TODO Auto-generated method stub
		entityManager.merge(player);
	}

	@Override
	public Player findPlayerByID(int id) {
		// TODO Auto-generated method stub
		return entityManager.find(Player.class, id);
	}

	@Override
	public void deletePlayer(Player player) {
		// TODO Modify the code
		entityManager.remove(entityManager.merge(player));
	}

	@Override
	public List<Player> getPlayers() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("SELECT c FROM Personne c", Player.class).getResultList();
	}
	
	@Override
	public List<Player> findAllPlayers() {
		Query q = entityManager.createQuery("SELECT c from Player c " , Personne.class); 
		return q.getResultList(); 
		
	}

	@Override
	public List<Player> getPlayersByAge(int age) {
		// TODO Auto-generated method stub
		TypedQuery<Player> query = entityManager.createQuery("SELECT c FROM Personne c WHERE c.age = :a" , Player.class);
		query.setParameter("a", age);
		return query.getResultList();
	}
	


	/*@Override
	public Player getPlayerByCIN(int cin) {
		TypedQuery<Player> query = entityManager.createQuery("SELECT c FROM Personne c WHERE c.cin = :a" , Player.class);
		query.setParameter("a", cin);
		return query.getSingleResult();
	}
*/
	@Override
	public Personne authenticate(String login, String password) {
		Personne found = null ; 
		String jpql = "select c from Personne c where c.login=:"+
		"login and c.password=:password"; 
		
		Query query = entityManager.createQuery(jpql); 
		query.setParameter("login", login); 
		query.setParameter("password", password); 
		
		try{
			found = (Personne) query.getSingleResult();
		}catch(Exception ex){
			return null ; 
		}
		
		return found ; 
	}

	
	
	
	

}
