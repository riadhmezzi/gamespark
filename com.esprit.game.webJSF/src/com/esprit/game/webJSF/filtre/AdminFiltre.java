package com.esprit.game.webJSF.filtre;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.esprit.game.webJSF.managedBeans.AuthenticationManagedBean;


/**
 * Servlet Filter implementation class AdminFiltre
 */
@WebFilter(filterName = "adminFiltre", urlPatterns = { "/pages/admin/*" })
public class AdminFiltre implements Filter {

    /**
     * Default constructor. 
     */
    public AdminFiltre() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest servletRequest = (HttpServletRequest) request ; 
		HttpServletResponse servletResponse = (HttpServletResponse) response ; 
		boolean letGo = false ;
		AuthenticationManagedBean mb = (AuthenticationManagedBean) servletRequest.getSession().getAttribute("AuthenticationMB");
		
		if(mb!=null){
		if( mb.getUserType().equals("Admin")&& mb.getLoggedIn()){
			letGo= true ; 
		}
		}
			
		if(letGo){
		chain.doFilter(request, response);
		}else{
			servletResponse.sendRedirect(servletRequest.getContextPath()+"/pages/welcome.jsf");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
