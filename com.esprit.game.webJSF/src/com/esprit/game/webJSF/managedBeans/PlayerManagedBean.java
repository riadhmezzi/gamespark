package com.esprit.game.webJSF.managedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.esprit.game.domain.Player;
import com.esprit.game.services.PlayerServiceLocal;





@ManagedBean(name="PlayerMB")
@ViewScoped
public class PlayerManagedBean  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Player player = new Player();
	
	private List<Player> players = new ArrayList<Player>();
	
	
	private boolean displayForm = false ; 
	private boolean displayListPlayer = true ; 
	public boolean isDisplayListPlayer() {
		return displayListPlayer;
	}


	public void setDisplayListPlayer(boolean displayListPlayer) {
		this.displayListPlayer = displayListPlayer;
	}


	private boolean displayMenu = true ;
	
	public boolean isDisplayForm() {
		return displayForm;
	}


	public void setDisplayForm(boolean displayMenu) {
		this.displayForm = displayMenu;
	}
	public boolean isDisplayMenu() {
		return displayMenu;
	}


	public void setDisplayMenu(boolean displayMenu) {
		this.displayMenu = displayMenu;
	}

	@PostConstruct
	public void init(){
		players = playerServiceLocal.findAllPlayers(); 
	}
	




	public Player getPlayer() {
		return player;
	}


	public void setPlayer(Player player) {
		this.player = player;
	}


	public List<Player> getPlayers() {
		return players;
	}


	public void setPlayers(List<Player> players) {
		this.players = players;
	}





	@EJB
	private PlayerServiceLocal playerServiceLocal; 
	
	public String saveOrUpdate(){
		String navTo=null ; 
		
		playerServiceLocal.updatePlayer(player);
		players = playerServiceLocal.findAllPlayers(); 
		displayForm = false ; 
		return navTo ;
	}
	
	public String showForm(){
		String navTo=null ; 
		player = new Player() ; 
		displayForm= true ; 
		
		return navTo ;
	}
	
	
	public String delete(){
		String navTo=null ; 
		playerServiceLocal.deletePlayer(player);
		players = playerServiceLocal.findAllPlayers();
		return navTo ;
	}
	
	public String cancel(){
		String navTo=null ; 
		displayForm = false; 
		return navTo ;
	}
	
	

	public String showList(){
		displayListPlayer=true;
		return null;
	}
		

	public String hideList(){
		displayListPlayer=false;
		return null;
	}

	

}
