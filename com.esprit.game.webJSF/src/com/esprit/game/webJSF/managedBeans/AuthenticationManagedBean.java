package com.esprit.game.webJSF.managedBeans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.esprit.game.domain.Admin;
import com.esprit.game.domain.Personne;
import com.esprit.game.domain.Player;
import com.esprit.game.services.PlayerServiceLocal;




@ManagedBean(name="AuthenticationMB")
@SessionScoped
public class AuthenticationManagedBean {

	@EJB
	private PlayerServiceLocal playerServiceLocal ; 
	
	private String userType ; 
	
	public String getUserType() {
		return userType;
	}


	public void setUserType(String userType) {
		this.userType = userType;
	}


	private Personne personne = new Personne () ; 
	
	private boolean loggedIn= false ; 
	
//	private String login ; 
//	private String password ;
	
	
	public boolean getLoggedIn() {
		return loggedIn;
	}


	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}


	public String authenticate(){
		String navTo= null ; 
		
		Personne found =  playerServiceLocal.authenticate(personne.getLogin(), personne.getPassword());
		
		
		
		if(found!=null){
			if (found instanceof Player){
				navTo="/pages/player/home"; 
				userType="Player";				
			
			}else if (found instanceof Admin){
				
				navTo="/pages/admin/manage";
				userType="Admin";
			}
				
			personne = found ; 
			loggedIn=true; 
		}else{
			loggedIn=false; 
			//navTo="/pages/error";
			FacesMessage message= new FacesMessage("Bad Credentials");
			FacesContext.getCurrentInstance().addMessage("loginForm:loginButton", message);
		}
				
		return navTo ; 
	}


	
	

	public Personne getPersonne() {
		return personne;
	}


	public void setPersonne(Personne personne) {
		this.personne = personne;
	}


	public String logOut(){
		String navTo = null ; 
		loggedIn= false ; 
		personne = new Personne(); 
		navTo="/pages/welcome";
		
		return navTo ; 
	}
	
	
	
	
//	public String getLogin() {
//		return login;
//	}
//	public void setLogin(String login) {
//		this.login = login;
//	}
//	public String getPassword() {
//		return password;
//	}
//	public void setPassword(String password) {
//		this.password = password;
//	} 
	
	
}
