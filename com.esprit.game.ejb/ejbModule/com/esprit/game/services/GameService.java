package com.esprit.game.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.esprit.game.domain.Game;

/**
 * Session Bean implementation class ClientService
 */
@Stateless
public class GameService implements GameServiceRemote, GameServiceLocal {
 
    @PersistenceContext(name="gamePU")
    EntityManager entityManager;

	@Override
	public void addGame(Game game) {
		// TODO Auto-generated method stub
		entityManager.persist(game);
	}

	@Override
	public void updateGame(Game game) {
		// TODO Auto-generated method stub
		entityManager.merge(game);
	}

	@Override
	public Game findGameByID(int id) {
		// TODO Auto-generated method stub
		return entityManager.find(Game.class, id);
	}

	@Override
	public void deleteGame(Game game) {
		// TODO Modify the code
		entityManager.remove(entityManager.merge(game));
	}


	
	@Override
	public List<Game> findAllGames() {
		Query q = entityManager.createQuery("SELECT c from Game c " , Game.class); 
		return q.getResultList(); 
		
	}


	

	
	
	
	
	

}
